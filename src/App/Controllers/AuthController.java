/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.State.DB;
import App.State.DQ;
import Utilities.Scan;

/**
 *
 * @author MYRV
 */
public class AuthController {
    
    public void serve() {
        var auth = DQ.Users.Login(Scan.Int("\r\n[INICIO] de Seción> "));
        if (auth == null) {
            System.out.println("(!) Usuario Invalido");
            return;
        }
        this.roleMenu();
    }
    
    private void roleMenu() {
        switch (DB.Auth.role) {
            case "Ciudadano"            -> { (new CitizensController()).serve(); }
            case "Oficial de Tránsito"  -> { (new OfficersController()).serve(); }
            case "Oficina de Juzgado"   -> { (new CourtController()).serve();    }
            default -> throw new AssertionError();
        }
    }
}
