/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.Models.Vehicle;
import App.State.DB;
import App.State.DQ;
import Utilities.Menu;
import Utilities.Scan;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class CitizensController {
    
    public void serve() {
        Menu.srv("Menu de Ciudadanos", this, new String[][]{
            { "Registro de vehículos",  "vehicleRegister" },
            { "Evento ciudadano",       "eventRegister"   },
        });
    }
    
    public void vehicleRegister() {
        if (DB.Auth.vehicles().size() >= 2) {
            System.out.println("(!) No puedes registrar más vehiculos");
            return;
        }
        var vehicle = DQ.Vehicles.newBuild(DB.Auth.id,  new String[]{
            "id", "year", "brand", "color", "type", 
        });
        System.out.println(vehicle.type + " " + vehicle.id + " registrado con exito");
    }
    
    public void eventRegister() {
        var vehicles = DB.Auth.vehicles();
        if (vehicles.isEmpty()) {
            System.out.println("(!) No posees vehiculos registrados");
            return;
        } 
        ArrayList<String> ids = new ArrayList<>();
        for (Vehicle vehicle : vehicles) ids.add(vehicle.id);
        var vehicle = DQ.Vehicles.find(Scan.From("Elije un vehicle:", ids));
        if (vehicle.event() != null) {
            System.out.println("(!) El vehiculos solo puede tener un evento registrado");
            return;
        }
        var event = DQ.Events.newBuild(vehicle, new String[]{
            "code", "province", "state", "created_at", "amount"
        });
        System.out.println("|| Evento " 
            + event.code + " " 
            + vehicle.type + "(" 
            + vehicle.id
            + ") registrado con exito\r\n||> "
            + "Monto: "
            + event.amount
        );
    }
    
}
