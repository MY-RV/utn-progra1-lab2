/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.Models.Event;
import App.State.DB;
import App.State.DQ;
import App.State.DS;
import Utilities.Menu;
import Utilities.Scan;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author MYRV
 */
public class CourtController {
    public void serve() {
        Menu.srv("Menu de Oficina Juzgado", this, new String[][]{
            { "Evento oficina del juzgado",  "courtEvents" },
            { "Reportes",                    "reports"     },
        });
    }
    
    public void courtEvents() {
        while (true) {
            var events = DQ.Events.where("state", "Por Aprobar");
            ArrayList<String> codes = new ArrayList<>();
            for (Event event : events) codes.add(event.code);
            codes.add("EXIT");

            String code = Scan.From("Eventos registrados: ", codes);
            if (code.equals("EXIT")) break;
            var event = DQ.Events.find(code);
            if (event == null) {
                System.out.println("(!) No se ha encontrado el evento");
                continue;
            }
            closeTicket(event);
            System.out.println("");
        }
    }
    
    private void closeTicket(Event event) {
        var vehicle = DQ.Vehicles.find(event.vehicle_id);
        var user    = DQ.Users.find(vehicle.user_id);
        var officer = DQ.Users.find(event.officer_id);

        System.out.println("\r\n"
            + "[EVENTO]" + "\r\n"
            + "    [Código]...." + event.code     + "\r\n"
            + "    [Número]....." + event.number   + "\r\n"
            + "    [Estado]....." + event.state    + "\r\n"
            + "    [Provincia].." + event.province + "\r\n"
            + "    [Monto]......" + event.amount   + "\r\n"
            + "    [Fecha]......" + event.date()   + "\r\n"

            + "[Perfil]" + "\r\n"
            + "    [Nombre]....." + user.name      + "\r\n"
            + "    [Cédula]....." + user.id        + "\r\n"
            + "    [Provincia].." + user.province  + "\r\n"
            
            + "[Oficial]" + "\r\n"
            + "    [Nombre]....." + officer.name  + "\r\n"
            + "    [Cédula]....." + officer.id    + "\r\n"

            + "[Vehiculo]" + "\r\n"
            + "    [Placa]......" + vehicle.id     + "\r\n"
            + "    [Tipo]......." + vehicle.type   + "\r\n"
        );
        while (true) {
            int ticket = Scan.Int("|> Número de Parte>");
            if (!DQ.Events.where("ticket", ticket).isEmpty()) {
                System.out.println("(!) El número de Parte ya fue registrado");
                continue;
            };
            event.ticket    = ticket;
            event.judge_id  = DB.Auth.id;
            event.state     = "Completado";
            break;
        }
        System.out.println("\r\n"
            + "[EVENTO]" + "\r\n"
            + "    [Código]....." + event.code     + "\r\n"
            + "    [Número]....." + event.number   + "\r\n"
            + "    [NParte]....." + event.ticket   + "\r\n"
            + "    [Estado]....." + event.state    + "\r\n"
            + "    [Provincia].." + event.province + "\r\n"
            + "    [Monto]......" + event.amount   + "\r\n"
            + "    [Fecha]......" + event.date()   + "\r\n"

            + "[Perfil]" + "\r\n"
            + "    [Nombre]....." + user.name      + "\r\n"
            + "    [Cédula]....." + user.id        + "\r\n"
            + "    [Provincia].." + user.province  + "\r\n"
            
            + "[Oficial]" + "\r\n"
            + "    [Nombre]....." + officer.name  + "\r\n"
            + "    [Cédula]....." + officer.id    + "\r\n"
                    
            + "[Juzgado]" + "\r\n"
            + "    [Nombre]....." + DB.Auth.name  + "\r\n"
            + "    [Cédula]....." + DB.Auth.id    + "\r\n"

            + "[Vehiculo]" + "\r\n"
            + "    [Placa]......" + vehicle.id     + "\r\n"
            + "    [Tipo]......." + vehicle.type   + "\r\n"
        );
        Scan.Enter();
    }
    
    public void reports() {
        Menu.srv("[REPORTES] Menu:", this, new String[][]{
            { "Por Aprobar(+45000)",  "report1" },
            { "Completados",          "report2" },
            { "Conteo por provincia", "report3" },
        });
    }
    
    public void report1() {
        var events = DQ.Events.where("state", "Por Aprobar");
        ArrayList<String> codes = new ArrayList<>();
        for (Event event : events) 
            if (event.amount > 45000) codes.add(event.code);
        codes.add("EXIT");

        String code = Scan.From("Eventos registrados: ", codes);
        var event = DQ.Events.find(code);
        if (event == null) {
            System.out.println("(!) No se ha encontrado el evento");
            return;
        }
        fullPrint(event);
    }
    
    public void report2() {
        var events = DQ.Events.where("state", "Completado");
        ArrayList<String> codes = new ArrayList<>();
        for (Event event : events) codes.add(event.code);
        codes.add("EXIT");

        String code = Scan.From("Eventos registrados: ", codes);
        var event = DQ.Events.find(code);
        if (event == null) {
            System.out.println("(!) No se ha encontrado el evento");
            return;
        }
        fullPrint(event);
    }
    
    public void report3() {
        var events = DB.Events;
        HashMap<String, Integer> Provinces = new HashMap<>(){{
            for (String key : DS.Provinces) put(key, 0);
        }};
        for (Event event : events) {
            int count = Provinces.get(event.province);
            Provinces.put(event.province, count+1);
        }
        System.out.println("[EVENTOS] por provincia:");
        for (Map.Entry<String, Integer> entry : Provinces.entrySet()) {
            var key = entry.getKey();
            var val = entry.getValue();
            System.out.println(key + " (" + val + ")");
        }
    }
    
    private static void fullPrint(Event event) {
        var vehicle = DQ.Vehicles.find(event.vehicle_id);
        var user = DQ.Users.find(vehicle.user_id);
        var officer = event.officer_id == null ? null
            : DQ.Users.find(event.officer_id);
        var judge = event.judge_id == null ? null
            : DQ.Users.find(event.judge_id);
        
        System.out.print("\r\n"
            + "[EVENTO]" + "\r\n"
            + "    [Código]....." + event.code     + "\r\n"
            + "    [Número]....." + event.number   + "\r\n"
            + "    [NParte]....." + event.ticket   + "\r\n"
            + "    [Estado]....." + event.state    + "\r\n"
            + "    [Provincia].." + event.province + "\r\n"
            + "    [Monto]......" + event.amount   + "\r\n"
            + "    [Fecha]......" + event.date()   + "\r\n"

            + "[PERFIL]" + "\r\n"
            + "    [Nombre]....." + user.name      + "\r\n"
            + "    [Cédula]....." + user.id        + "\r\n"
            + "    [Provincia].." + user.province  + "\r\n"
        );
        if (officer != null) System.out.print("[OFICIAL]\r\n"
            + "    [Nombre]....." + officer.name  + "("
            + officer.id + ")\r\n"
        );
        if (judge   != null) System.out.print("[JUZGADO]\r\n"
            + "    [Nombre]....." + judge.name  + "(" 
            + judge.id + ")\r\n"
        );         
        System.out.println("[VEHICULO]\r\n"
            + "    [Placa]......" + vehicle.id     + "\r\n"
            + "    [Tipo]......." + vehicle.type   + "\r\n"
            + "    [Año]........" + vehicle.year   + "\r\n"
            + "    [Color]......" + vehicle.color   + "\r\n"
            + "    [Marca]......" + vehicle.brand   + "\r\n"
        );
    }
    
}
