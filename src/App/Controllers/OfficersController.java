/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.Models.Event;
import App.State.DB;
import App.State.DQ;
import Utilities.Menu;
import Utilities.Scan;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class OfficersController {
    
    public void serve() {
        Menu.srv("Menu de Oficiales", this, new String[][]{
            { "Evento oficial de tránsito",  "officerEvents" },
        });
    }
    
    public void officerEvents() {
        while (true) {
            var events = DQ.Events.where("state", "Abierto");
            ArrayList<String> codes = new ArrayList<>();
            for (Event event : events) codes.add(event.code);
            codes.add("EXIT");

            String code = Scan.From("Eventos registrados: ", codes);
            if (code.equals("EXIT")) break;
            var event = DQ.Events.find(code);
            if (event == null) {
                System.out.println("(!) No se ha encontrado el evento");
                continue;
            }
            aprobateTicket(event);
            System.out.println("");
        }
    }
    
    private void aprobateTicket(Event event) {
        var vehicle = DQ.Vehicles.find(event.vehicle_id);
        var user = DQ.Users.find(vehicle.user_id);

        System.out.println("\r\n"
            + "[EVENTO]"       + "\r\n"
            + "    [Código]....." + event.code     + "\r\n"
            + "    [Estado]....." + event.state    + "\r\n"
            + "    [Provincia].." + event.province + "\r\n"
            + "    [Monto]......" + event.amount   + "\r\n"
            + "    [Fecha]......" + event.date()   + "\r\n"

            + "[Perfil]"       + "\r\n"
            + "    [Nombre]....." + user.name      + "\r\n"
            + "    [Cédula]....." + user.id        + "\r\n"
            + "    [Provincia].." + user.province  + "\r\n"

            + "[Vehiculo]" + "\r\n"
            + "    [Placa]......" + vehicle.id     + "\r\n"
            + "    [Tipo]......." + vehicle.type   + "\r\n"
        );
        while (true) {
            int number = Scan.Int("|> Número de Ticket>");
            if (!DQ.Events.where("number", number).isEmpty()) {
                System.out.println("(!) El número de Evento ya fue registrado");
                continue;
            };
            event.number = number;
            event.officer_id = DB.Auth.id;
            event.state = "Por Aprobar";
            break;
        }
        System.out.println("\r\n"
            + "[EVENTO]" + "\r\n"
            + "    [Código]....." + event.code     + "\r\n"
            + "    [Número]....." + event.number   + "\r\n"
            + "    [Estado]....." + event.state    + "\r\n"
            + "    [Provincia].." + event.province + "\r\n"
            + "    [Monto]......" + event.amount   + "\r\n"
            + "    [Fecha]......" + event.date()   + "\r\n"

            + "[Perfil]" + "\r\n"
            + "    [Nombre]....." + user.name      + "\r\n"
            + "    [Cédula]....." + user.id        + "\r\n"
            + "    [Provincia].." + user.province  + "\r\n"
            
            + "[Oficial]" + "\r\n"
            + "    [Nombre]....." + DB.Auth.name  + "\r\n"
            + "    [Cédula]....." + DB.Auth.id    + "\r\n"

            + "[Vehiculo]" + "\r\n"
            + "    [Placa]......" + vehicle.id     + "\r\n"
            + "    [Tipo]......." + vehicle.type   + "\r\n"
        );
        Scan.Enter();
    }
    
}
