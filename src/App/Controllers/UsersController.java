/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Controllers;

import App.State.DQ;

/**
 *
 * @author MYRV
 */
public class UsersController {

    public void create() {
        var user = DQ.Users.newBuild(new String[]{
            "id", "name", "province", "role"
        });
        System.out.println("Usuario " + user.name + "(" + user.id + ") creado con EXITO");
    }

}
