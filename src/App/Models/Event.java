/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Models;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MYRV
 */
public class Event {
    public Integer amount, number, ticket;
    public Integer officer_id, judge_id;
    public String code, province, state, vehicle_id;
    public Date created_at;
    
    private final SimpleDateFormat FDate = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    public String date() { return FDate.format(this.created_at); }
    
}
