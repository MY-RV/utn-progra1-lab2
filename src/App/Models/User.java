package App.Models;

import App.State.DQ;
import java.util.ArrayList;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class User {
    public Integer  id;
    public String   name, role, province;
    
    public ArrayList<Vehicle> vehicles() {
        return DQ.Vehicles.where("user_id", this.id);
    }
    
}
