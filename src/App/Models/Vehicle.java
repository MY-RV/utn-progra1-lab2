/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.Models;

import App.State.DQ;

/**
 *
 * @author MYRV
 */
public class Vehicle {
    public Integer user_id, year;
    public String id, type, brand, color;
    
    public Event event() {
        var result = DQ.Events.where("vehicle_id", this.id);
        if (result.isEmpty()) return null;
        return result.get(0);
    }
    
}
