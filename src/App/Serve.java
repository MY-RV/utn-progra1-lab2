/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App;

import Utilities.Menu;

/**
 *
 * @author MYRV
 */
public class Serve {
    
    private static final Menu LandMenu = Menu.gen("MYRV LabI(2022/PI):", null, 
        new String[][]{
            { "Inicio de Sesión",       "App.Controllers.AuthController::serve"   },
            { "Registro de Perfiles",   "App.Controllers.UsersController::create" },
        }
    ); 
    
    public static void newApp() {
        LandMenu.serve();
    }
    
}
