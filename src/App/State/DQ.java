/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State;

import App.State.Queries.QEvents;
import App.State.Queries.QUsers;
import App.State.Queries.QVehicles;

/**
 *
 * @author MYRV
 */
public class DQ {
    public static QUsers    Users = new QUsers();
    public static QVehicles Vehicles = new QVehicles();
    public static QEvents   Events = new QEvents();
}
