/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State;

/**
 *
 * @author MYRV
 */
public class DS {
    public static final String[] Roles = new String[]{
        "Ciudadano", "Oficial de Tránsito", "Oficina de Juzgado"
    };
    
    public static final String[] Provinces = new String[]{
        "San José", "Alajuela", "Heredia", "Cartago", 
        "Puntarenas", "Guanacaste", "Limon"
    };
    
    public static final String[] States = new String[]{
        "Abierto", "Por Aprobar", "Completado"
    };
    
    public static final String[] VehicleTypes = new String[]{
        "Moto", "Automóvil", "Bus", "Camión"
    };
    
}
