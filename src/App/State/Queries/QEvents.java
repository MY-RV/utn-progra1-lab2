/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Queries;

import App.Models.Event;
import App.Models.Vehicle;
import App.State.DB;
import App.State.DS;
import Utilities.Faker;
import Utilities.Scan;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author MYRV
 */
public class QEvents { 
    public Integer calc(Vehicle vehicle) { /* Preguntar */
        int amount;
        switch (vehicle.type) {
            case "Moto"      -> amount = (10000 * 115) / 100;
            case "Automóvil" -> amount = (25000 * 130) / 100;
            case "Bus"       -> amount = (45000 * 145) / 100;
            case "Camión"    -> amount = (65000 * 170) / 100;
            default          -> amount = 0;
        }
        if (vehicle.year < 2000) amount *= 1.10f;
        return amount;
    }
    
    public Event find(String code) {
        for (var event : DB.Events) if (code.equals(event.code)) return event;
        return null;
    }
    
    public ArrayList<Event> where(String attr, String value) {
        ArrayList<Event> response = new ArrayList<>();
        for (Event row : DB.Events) {
            switch (attr) {
                case "vehicle_id" -> {if (row.vehicle_id.equals(value)) {
                    response.add(row);
                }}
                case "state" -> {if (row.state.equals(value)) {
                    response.add(row);
                }}
                default -> throw new AssertionError(attr);
            }
        }
        return response;
    }
    
    public ArrayList<Event> where(String attr, Integer value) {
        ArrayList<Event> response = new ArrayList<>();
        for (Event row : DB.Events) {
            switch (attr) {
                case "number" -> {if (row.number == value) {
                    response.add(row);
                }}
                case "ticket" -> {if (row.ticket == value) {
                    response.add(row);
                }}
                default -> throw new AssertionError(attr);
            }
        }
        return response;
    }
    
    public Event newBuild(Vehicle vcl, String[] attrs) {
        System.out.println("[CREACIÓN de perfiles]");
        var event = new Event(){{
            this.vehicle_id = vcl.id;
            for (String attr : attrs) inputIn(attr, this);
        }};
        DB.Events.add(event);
        return event;
    }
    
    public Event inputIn(String attr, Event event) {
        switch (attr.toLowerCase()) {
            case "code" -> 
                event.code = Faker.UniqueStr(attr, 10);
            case "province" -> 
                event.province = Scan.From("Provincia:", DS.Provinces);
            case "state" -> 
                event.state = "Abierto";
            case "created_at" -> 
                event.created_at = new Date();
            case "amount" -> 
                event.amount = calc((new QVehicles()).find(event.vehicle_id));
            default -> throw new AssertionError(attr);
        }
        return event;
    }
    
}
