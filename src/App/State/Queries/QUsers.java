/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Queries;

import App.Models.User;
import App.Models.Vehicle;
import App.State.DB;
import App.State.DS;
import Utilities.Scan;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class QUsers {
    public User find(int id) {
        for (var user : DB.Users) if (user.id == id) return user;
        return null;
    }
    
    public ArrayList<User> where(String attr, String value) {
        ArrayList<User> response = new ArrayList<>();
        for (User row : DB.Users) {
            switch (attr) {
                case "role" -> {if (row.role.equals(value)) {
                    response.add(row);
                }}
                default -> throw new AssertionError(attr);
            }
        }
        return response;
    }
    
    public User Login(int id) {
        DB.Auth = find(id);
        return DB.Auth;
    }
    public void Logout() {
        DB.Auth = null;
    }
    
    public User newBuild(String[] attrs) {
        System.out.println("[CREACIÓN de perfiles]");
        var user = new User();
        for (String attr : attrs) inputIn(attr, user);
        DB.Users.add(user);
        return user;
    }
    
    public User inputIn(String attr, User user) {
        switch (attr.toLowerCase()) {
            case "id" -> {while (true) {
                user.id = Scan.Int("Indentificación> ");
                if (find(user.id) == null) break;
                System.out.println("(!) El ID ya esta en uso.\r\n");
            }}
            case "name" -> 
                user.name = Scan.Str("Nombre> ");
            case "province" -> 
                user.province = Scan.From("Provincia:", DS.Provinces);
            case "role" -> // BadCallable //
                user.role = Scan.From("Role:", DS.Roles);
            default -> throw new AssertionError(attr);
        }
        return user;
    }
    
}
