/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Queries;

import App.Models.User;
import App.Models.Vehicle;
import App.State.DB;
import App.State.DS;
import Utilities.Scan;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class QVehicles {
    
    public Vehicle find(String id) {
        for (var vehicle : DB.Vehicles) {
            if (vehicle.id == null ? id == null : vehicle.id.equals(id)) 
                return vehicle;
        }
        return null;
    }
    
    public ArrayList<Vehicle> where(String attr, int value) {
        ArrayList<Vehicle> response = new ArrayList<>();
        for (Vehicle row : DB.Vehicles) {
            switch (attr) {
                case "user_id" -> {if (row.user_id == value) {
                    response.add(row);
                }}
                default -> throw new AssertionError(attr);
            }
        }
        return response;
    }
    
    public Vehicle newBuild(int owner, String[] attrs) {
        System.out.println("[CREACIÓN de perfiles]");
        var vehicle = new Vehicle(){{ 
            for (String attr : attrs) inputIn(attr, this);
            this.user_id = owner; }};
        DB.Vehicles.add(vehicle);
        return vehicle;
    }
    
    public Vehicle inputIn(String attr, Vehicle vehicle) {
        switch (attr.toLowerCase()) {
            case "id" -> {while (true) {
                vehicle.id = Scan.Str("Placa> ");
                if (find(vehicle.id) == null) break;
                System.out.println("(!) La Placa ya esta en uso.\r\n");
            }}
            case "type" -> 
                vehicle.type = Scan.From("Tipo:", DS.VehicleTypes);
            case "year" -> 
                vehicle.year = Scan.Int("Año> ");
            case "brand" -> 
                vehicle.brand = Scan.Str("Marca> ");
            case "color" -> 
                vehicle.color = Scan.Str("Color> ");
            default -> throw new AssertionError(attr);
        }
        return vehicle;
    }
}
