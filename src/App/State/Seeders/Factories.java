package App.State.Seeders;

import App.State.Seeders.Factory.FEvents;
import App.State.Seeders.Factory.FUsers;
import App.State.Seeders.Factory.FVehicles;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class Factories {
    public static void run() {
        FUsers.gen(80);
        FVehicles.gen(40);
        FEvents.gen(20);
    }
}
