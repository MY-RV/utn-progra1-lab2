/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Seeders.Factory;

import App.Models.Event;
import App.Models.User;
import App.Models.Vehicle;
import App.State.DB;
import App.State.DQ;
import App.State.DS;
import Utilities.Faker;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author MYRV
 */
public class FEvents {
    
    public static ArrayList<User> officers = DQ.Users.where("role", "Oficial de Tránsito");
    public static ArrayList<User> judges = DQ.Users.where("role", "Oficina de Juzgado");
    
    public static void gen(int tms) { 
        if (DB.Vehicles.size() < tms) tms = DB.Vehicles.size();
        for (int i = 0; i < tms; i++) gen(); 
    }
    public static void gen() {
        ArrayList<Vehicle> vcls = new ArrayList<>();
        for(Vehicle vcl : DB.Vehicles) if(vcl.event() == null) vcls.add(vcl);
        DB.Events.add(new Event(){{
            this.code       = Faker.UniqueStr("event.code", 10);
            this.state      = Faker.RndFrom(DS.States);
            this.province   = Faker.RndFrom(DS.Provinces);
            this.vehicle_id = Faker.RndFrom(vcls).id;
            this.amount     = DQ.Events.calc(DQ.Vehicles.find(this.vehicle_id));
            this.created_at = new Date();
            
            if (!"Abierto".equals(state)) {
                this.officer_id = Faker.RndFrom(officers).id;
                this.number = Faker.UniqueInt("event.number", 1000, 10000);
            }
            if ("Completado".equals(state)) {
                this.judge_id = Faker.RndFrom(judges).id;
                this.ticket = Faker.UniqueInt("event.ticket", 1000, 10000);
            }
        }});
    }
    
}
