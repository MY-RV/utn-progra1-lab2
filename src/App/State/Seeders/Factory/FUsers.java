/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Seeders.Factory;

import App.Models.User;
import App.State.DB;
import App.State.DS;
import Utilities.Faker;

/**
 *
 * @author MYRV
 */
public class FUsers {

    public static void gen(int tms) { for (int i = 0; i < tms; i++) gen(); }
    public static void gen() {
        DB.Users.add(new User(){{
            this.id         = Faker.UniqueInt("user.id", 100000000, 1000000000);
            this.name       = Faker.UniqueStr("user.name");
            this.province   = Faker.RndFrom(DS.Provinces);
            this.role       = Faker.RndFrom(DS.Roles);
        }});
    }
    
}
