/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package App.State.Seeders.Factory;

import App.Models.User;
import App.Models.Vehicle;
import App.State.DB;
import App.State.DQ;
import App.State.DS;
import Utilities.Faker;
import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class FVehicles {
    
    public static void gen(int tms) { for (int i = 0; i < tms; i++) gen(); }
    public static void gen() {
        ArrayList<User> users = new ArrayList<>();
        ArrayList<User> citizens = DQ.Users.where("role", "Ciudadano");
        for(User usr : citizens) if(usr.vehicles().size() < 2) users.add(usr);
        DB.Vehicles.add(new Vehicle(){{
            this.id     = Faker.UniqueStr("vehicle.id", 3) + "-" + 
                Faker.UniqueInt("vehicle.id", 100, 1000);
            this.type   = Faker.RndFrom(DS.VehicleTypes);
            this.user_id= Faker.RndFrom(users).id;
            this.year   = Faker.Int(1980, 2023);
            this.brand  = Faker.Str();
            this.color  = Faker.RndFrom(new String[]{
                "Red", "Green", "Blue", "Orange", "White", "Black", "Gray"
            });
        }});
    }
    
}
