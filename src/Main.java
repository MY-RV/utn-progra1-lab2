
import App.State.Seeders.Factories;
import javax.swing.JOptionPane;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class Main {
    
    public static void main(String[] args) {        
        int option = JOptionPane.showConfirmDialog(null, "Run Seeders?");
        if (option == 2) return;
        else if (option == 0) Factories.run();
        App.Serve.newApp(); 
    }
    
}
