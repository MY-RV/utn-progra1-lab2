/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author MYRV
 */
public class Dinamic {
    protected static final String prPackage = "";
    
    //<editor-fold defaultstate="collapsed" desc="PUBLIC">
    public static void exec(String method) {
        String out[] = method.split("::", 2);
        Object obj = getObject(out[0]);
        execMethod(obj, out[1]);
    }
    
    public static void execMethod(Object obj, String name) {
        try {
            getMethod(obj, name).invoke(obj);
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException e) {
            System.out.println(e);
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="PRIVATE">
    private static Method getMethod(Object obj, String name) {
        try {
            return obj.getClass().getMethod(name);
        }
        catch (SecurityException | NoSuchMethodException e) {
            System.out.println(e);
        }
        return null;
    }
    
    private static Object getObject(String name) {
        try {
            Class<?> className = Class.forName(prPackage + "" + name);
            return className.getConstructor().newInstance();
        } catch (
                SecurityException |
                        NoSuchMethodException |
                        ClassNotFoundException |
                        InstantiationException |
                        IllegalAccessException |
                        IllegalArgumentException |
                        InvocationTargetException ex
                ) {
            Logger.getLogger(Dinamic.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    //</editor-fold>
    
}
