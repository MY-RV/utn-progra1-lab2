/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author MYRV
 */
public class Faker {
    private static final Random Rnd = new Random();
    private static final String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";//"12345674890";
    private static final Set<String> identifiers = new HashSet<>();
    
    private static final HashMap<String, ArrayList<String>> uniqueStr = new HashMap<>();
    private static final HashMap<String, ArrayList<Integer>> uniqueInt = new HashMap<>();
    
    public static String UniqueStr(String key, int length) {
        ArrayList<String> list = getFrom(key, uniqueStr);
        while (true) {
            String value = RndLex(length);
            if (existIn(value, list)) continue;
            list.add(value);
            return value;
        }
    }
    
    public static String UniqueStr(String key) {
        return UniqueStr(key, Rnd.nextInt(5)+5);
    }
    
    public static Integer UniqueInt(String key, int from, int to) {
        ArrayList<Integer> list = getFrom(key, uniqueInt);
        while (true) {
            Integer value = ThreadLocalRandom.current().nextInt(from, to);
            if (existIn(value, list)) continue;
            list.add(value);
            return value;
        }
    }
    
    public static String Str(int length) {
        return RndLex(length);
    }
    
    public static String Str() {
        return Str(Rnd.nextInt(5)+5);
    }
    
    public static Integer Int(int from, int to) {
        return ThreadLocalRandom.current().nextInt(from, to);
    }
    
    public static <T>T RndFrom(ArrayList<T> list) {
        int count = list.size();
        return list.get(Rnd.nextInt(count));
    }
    public static <T>T RndFrom(T[] array) {
        int count = array.length;
        return array[Rnd.nextInt(count)];
    }
    
    private static <ArrType>ArrType getFrom(String key, HashMap<String, ArrType> map) {
        if (map.get(key) == null) map.put(key, (ArrType) new ArrayList<>());
        return map.get(key);
    }
    
    private static <T>Boolean existIn(T value, ArrayList<T> list) {
        for (T row : list) if (row == value) return true;
        return false;
    }
    
    private static String RndLex(int length) {
        StringBuilder builder = new StringBuilder();
        while(builder.toString().length() == 0) {
            for(int i = 0; i < length; i++) 
                builder.append(lexicon.charAt(Rnd.nextInt(lexicon.length())));
            if(identifiers.contains(builder.toString())) builder = new StringBuilder();
        }
        return builder.toString();
    }
}
