/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

import java.util.ArrayList;

/**
 *
 * @author MYRV
 */
public class Menu {
    private final Object controller;
    private final String menu, callbacks[], options[];
    //<editor-fold defaultstate="collapsed" desc="QUICK BUILD/SERVE">
    public static void srv(String title, Object ctrl, String cbacks[][]) {
        (new Menu(title, ctrl, cbacks)).serve();
    }
    public static Menu gen(String title, Object ctrl, String cbacks[][]) {
        return new Menu(title, ctrl, cbacks);
    }
    //</editor-fold>
    
    public Menu(String title, Object ctrl, String cbacks[][]) {
        this.controller = ctrl;
        this.callbacks  = getVals(cbacks, 1);
        this.options    = getVals(cbacks, 0);
        this.menu       = genMenu(title, this.options);
    }
    
    public void serve() {
        Scan.Clear();
        String selected;
        Boolean breaking = false;
        
        while (true) {
            if (breaking) Scan.Enter();
            System.out.println(this.menu);
            breaking = true;
            
            selected = Scan.Str("Select> ").toLowerCase();
            if ("e".equals(selected)) break;
            else if (!Valid.Int(selected)) continue;
            
            int index = Integer.parseInt(selected) - 1;
            if (index >= this.options.length || index < 0) continue;
            
            if (this.controller == null) Dinamic.exec(this.callbacks[index]);
            else Dinamic.execMethod(this.controller, this.callbacks[index]);
        } 
    }
    
    //<editor-fold defaultstate="collapsed" desc="UTILITY">
    private String[] getVals(String callbacks[][], int value) {
        ArrayList<String> response = new ArrayList<>();
        for (String[] callback : callbacks) response.add(callback[value]);
        return response.toArray(String[]::new);
    }
    
    private String genMenu(String title, String options[]) {
        for (int i = 0; i < options.length; i++)
        { title += "\r\n  " + (i+1) + ") " + options[i]; }
        return title + "\r\n  E) EXIT";
    }
    //</editor-fold>
    
}
