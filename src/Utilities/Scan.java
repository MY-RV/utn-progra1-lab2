package Utilities;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author MYRV
 */
public class Scan {
    private static String newScan() { return (new Scanner(System.in)).nextLine(); }
    public static void Enter() { 
        newScan();
        Clear();
    }
    public static void Clear() {
        try {
            Runtime.getRuntime().exec((
                System.getProperty("os.name").contains("Windows")
                ? "cls" 
                : "clear"
            ));
        } catch (final Exception e) { System.out.println("\r\n\r\n< - O - 0 - O ->\r\n\r\n"); } 
    }
    
    //<editor-fold defaultstate="collapsed" desc="BASIC SCANS">
    public static String Str(String TEXT) {
        System.out.print(TEXT);
        return newScan();
    }
    
    public static float  Flt(String TEXT) {
        System.out.print(TEXT);
        return Float.parseFloat(newScan());
    }
    
    public static int    Int(String TEXT) {
        System.out.print(TEXT);
        String str = newScan().strip();
        if ("".equals(str)) return 0;
        return Integer.parseInt(str);
    }
    
    public static Date  Date(String TEXT) {
        System.out.print(TEXT);
        return new Date(newScan());
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="COMPLEX SCANS">
    public static String From(String TEXT, String[] optns) {
        System.out.println(TEXT);
        for (int i = 0; i < optns.length; i++) System.out.println(
                "  " + (i+1) + ") " + cap(optns[i])
        );
        while (true) {
            int idx = Scan.Int("Select> ");
            if (!(idx > optns.length || idx <= 0)) return optns[idx-1];
        }
    }
    
    public static String From(String TEXT, ArrayList<String> list) {
        return From(TEXT, list.toArray(String[]::new));
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="UTILITIES">
    private static String cap(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
    //</editor-fold>
    
}
