/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Utilities;

/**
 *
 * @author MYRV
 */
public class Valid {
    
    public static boolean Int(String s) {
        try { Integer.valueOf(s); } 
        catch(NumberFormatException | NullPointerException e) { return false; }
        return true;
    }
    
}
